#!/usr/bin/env Rscript
###
# Name:        unknown-authors.py
# Description: Move author from P2093 to a specific element in Wikidata
#
# Notes:

# Author: Miguel Tremblay
# Date: 16 août 2019
###



# URL is of type "http://www.wikidata.org/entity/Q180686"
get_element_from_url <- function(sURL){

        if (is.na(sURL)) {return ("")}
        
	# Get element after the last '/'
	l <- strsplit(sURL, '/')
	sElement <- tail(l[[1]], n=1)

	return (sElement)
}


# Generate the QS command to delete the string from P2093
del_author  <- function(sStringAuthor, sElementArticle){
 
	sElementArticle <- paste("-", sElementArticle, sep="")
	sStringAuthor <- paste('"',sStringAuthor, '"', sep="") 
	sQuickStatementDel <- paste(c(sElementArticle, "P2093",sStringAuthor ),
		 collapse='\t')

	return (sQuickStatementDel)
}

# Generate the QS to add the information into author
add_author <- function(sElementAuthor, sElementArticle, nOrder, 
		sAffirmeDans, nID, sDate, sURL){

	sQuickStatementAdd <- paste(c(sElementArticle, "P50",sElementAuthor),
		 collapse='\t')

	if (length(nOrder) > 0){
	nOrder <- paste('"', nOrder, '"', sep="")
	sQuickStatementAdd <- paste(c(sQuickStatementAdd,"P1545",nOrder),
		 collapse='\t' )
	}
	if (length(sAffirmeDans) > 0 && sAffirmeDans != ""){
	sQuickStatementAdd <- paste(c(sQuickStatementAdd, "S248", sAffirmeDans),
		 collapse='\t' )
	}
	if (!is.na(nID) && nID != ""){
	nID <- paste('"', nID, '"', sep="")
	sQuickStatementAdd <- paste(c(sQuickStatementAdd, "S698", nID), 
		collapse='\t' )
	}
	if ( !is.na(sURL) && sURL != "" ){
	sURL <- paste('"', sURL, '"', sep="")
	sQuickStatementAdd <- paste(c(sQuickStatementAdd, "S854", sURL), 
		collapse='\t')
	}
	if (!is.na(sDate) && sDate != ""){
	sDate <- paste("+", sDate, "/11", sep="")
	sQuickStatementAdd <- paste(c(sQuickStatementAdd, "S813", sDate ),  
		collapse='\t')
	}


	return (sQuickStatementAdd)
}

#######

args <- commandArgs(trailingOnly=TRUE)

# test if there is at least one argument: if not, return an error
sOutput <- NULL
if (length(args)<2) {
  stop("At least two arguments must be supplied: authorName WD:Element.n", 
		call.=FALSE)
} else if (length(args) == 3){
   sOutput <- args[3]
}
sAuthorString <- args[1]
sElementAuthors <- args[2]


# This one list only the article with a reference
sparql_query <- paste('SELECT ?item ?auteur ?ordre ?affirme ?id ?date ?url
{
  ?item p:P2093 ?chaine .
  ?chaine ps:P2093 "', sAuthorString, '";
          ps:P2093 ?auteur ;
          prov:wasDerivedFrom ?from .
optional {?chaine pq:P1545 ?ordre . }
optional {?from pr:P248 ?affirme . }
optional {?from pr:P698 ?id . }
optional {?from pr:P813 ?date . }
optional {?from pr:P854 ?url . }
}', sep="")

# For an article with reference, use a second request
sparql_query2 <- paste('SELECT ?item ?auteur ?ordre ?affirme ?id ?date ?url
{
  ?item p:P2093 ?chaine .
  ?chaine ps:P2093 "', sAuthorString, '";
          ps:P2093 ?auteur ;
  optional {?chaine pq:P1545 ?ordre . }
}', sep="")


w <- WikidataQueryServiceR::query_wikidata(sparql_query)
w2 <- WikidataQueryServiceR::query_wikidata(sparql_query2)
out <- file(sOutput, 'w')
request_list<-list(w,w2)

for (sparql in request_list){
    if (length(sparql) == 0) { next }
    if (nrow(sparql) > 0){
        for (nLigne in seq(1,nrow(sparql))){
            sArticle <- get_element_from_url(sparql[nLigne,1])
            nOrdre <- sparql[nLigne,3]            
            # Case of reference
            if( ncol(sparql) > 2 ) {
                sAffirme <- get_element_from_url(sparql[nLigne,4])
                nID <- sparql[nLigne,5]
                sDate <- sparql[nLigne,6]
                sURL <- sparql[nLigne,7]
            }
            else { sAffirme = nID = sDate = sURL = "" }
            sAddQS <- add_author(sElementAuthors, sArticle,nOrdre,sAffirme,
                nID,sDate,sURL)
            write(sAddQS, file=sOutput, append=TRUE)
            sDelQS <- del_author(sAuthorString, sArticle)
            write(sDelQS, file=sOutput, append=TRUE)	 
        }
    }
}

if (is.null(sOutput)){
	print(lQS)
}
#else{	
	#	writeLines(lQS, sOutput)
#	write.list(lQS, file=sOutput, append=True)
#}
