P2093-to-P50
=============

Move author from P2093 to P50 with corresponding qualifiers and references using [QuickStatements](https://www.wikidata.org/wiki/Help:QuickStatements).

Introduction
------------

Property P2093 contains the names of authors as strings, ie. they do not correspond to any element. Once an element is created, or identify, for a specific author, this R script allow to move all (or some) reference from P2093 to P50, keeping all attributes and references.

Requirements
------------

* [R](https://www.r-project.org/) 
* [WikidataQueryServiceR](https://cran.r-project.org/web/packages/WikidataQueryServiceR/index.html)


Usage
-----

Assign all article for "Charlotte Delay" to element Q66480654 and write the QuickStatement commands in out.txt
```bash
 Rscript P2093-to-P50.r "Charlotte Delay" Q66480654 out.txt
 ```
<br />

SPARQL
-----

Two separates SPARQL requests must be used, since I did not find a way to have the references as an optional statement. The first one manage the authors with references, and the second without references.

```bash
#Author name strings with references
SELECT ?item ?auteur ?ordre ?affirme ?id ?date ?url
{
  ?item p:P2093 ?chaine .
  ?chaine ps:P2093 "Charlotte Delay" ;
          ps:P2093 ?auteur ;
          prov:wasDerivedFrom ?from .
optional {?chaine pq:P1545 ?ordre . }
optional {?from pr:P248 ?affirme . }
optional {?from pr:P698 ?id . }
optional {?from pr:P813 ?date . }
optional {?from pr:P854 ?url . }
}
```
[Try it!](https://w.wiki/7Cs)

```bash
#Author name strings without references
SELECT ?item ?auteur ?ordre ?affirme ?id ?date ?url
{
  ?item p:P2093 ?chaine .
  ?chaine ps:P2093 "Charlotte Delay" ;
          ps:P2093 ?auteur ;
   optional {?chaine pq:P1545 ?ordre . }
}
```
[Try it!](https://w.wiki/7DW)

Output
-----

An example of QuickStatement output can be seen in the file [quickstatement.txt](quickstatement.txt).

Bugs
-----

For any bug report, please contact [P2093-to-P50.miguel@ptaff.ca](mailto:P2093-to-P50.miguel@ptaff.ca)

Author
-----

[Miguel Tremblay](http://ptaff.ca/miguel/)

License
-----

Copyright © 201P2093-to-P50 Miguel Tremblay.

P2093-to-P50 is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.